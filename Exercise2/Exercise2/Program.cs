﻿using System;

namespace Exercise2
{
    class Program
    {
        static void Main(string[] args)
        {
            var myName = "Fiona";
            var greeting = $"Hello and welcome my name is {myName}, it is a pleasure to meet you.";

            Console.WriteLine(greeting);
        }
    }
}
